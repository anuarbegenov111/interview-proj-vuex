import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'
import router from '../router/index.js'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        posts: []
    },

    getters: {},

    actions: {
        getPosts({ commit }) {
             axios.get('https://jsonplaceholder.typicode.com/posts')
                .then(response => {
                    commit('SET_POSTS', response.data)
                })
        },
        deletePost({commit}, id) {
            axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
                .then(() => commit('DELETE_POST', id))
                
        },
        createPost({commit}, post) {
            axios.post('https://jsonplaceholder.typicode.com/posts', post)
                .then((response) => {
                    if (response.status === 201) {
                        router.push({path : '/'})
                    }
                })
                .then(() => commit('CREATE_POST', post))
        },
        updatePost({commit}, id, post) {
            axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`, post)
                .then(() => commit())
        }

    },

    mutations: {
        SET_POSTS(state, posts) {
            state.posts = posts
        },
        DELETE_POST(state, id) {
            const index = state.posts.findIndex(post => post.id == id)
            state.posts.splice(index, 1)
        },
        CREATE_POST(state, post) {
            state.posts = post
        },
        UPDATE_POST(state, updPost, id) {
            state.posts = state.posts.map(post => {
                if (post.id === id) {
                  return Object.assign({}, post, updPost)
                }
                return post
              })
        }
    }
})
